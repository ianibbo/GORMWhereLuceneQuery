package gormfindbylqs

// import grails.gorm.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.BooleanClause
import org.apache.lucene.queryparser.classic.QueryParser;
import groovy.util.logging.Slf4j;
import grails.util.GrailsClassUtils;
import org.hibernate.criterion.*;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.hibernate.sql.JoinType

@Slf4j
public class LuceneDetachedQueryBuilder {

  private static class PropertyDef extends HashMap<String, String> {
    PropertyDef () {
      super()
    }

    @Override
    public String toString () {
      String al = this.get('alias')
      "${al ? al + '.' : ''}${this.get('property')}".toString()
    }

    public Object asType(Class type) {
      if (type == String) {
        return this.toString()
      }

      super.asType(type)
    }
  }

  public static DetachedCriteria build(PersistentEntity pe, String lucene_query_string) {
    log.debug("LuceneDetachedQueryBuilder::build(${pe},${lucene_query_string})");
    Class c = pe.getJavaClass();
    DetachedCriteria dc = grails.util.GrailsClassUtils.getStaticPropertyValue(c, 'detachedCriteria') as DetachedCriteria;

    if ( dc == null ) {
      log.debug("class ${c.name} does not respond to getDetachedCriteria - set up default");
      dc = DetachedCriteria.forClass(c);
    } 
    else { 
      log.debug("class ${c.name} does respond to getDetachedCriteria.. continue");
    }

    return build(dc, pe, lucene_query_string);
  }


  /**
   * Decorate the base query with parameters that reflect the specification given in the lucene query string
   * based on the configuration availabe in the persistent entity about how different properties are to be
   * interpreted.
   */
  public static DetachedCriteria build(DetachedCriteria dc, PersistentEntity pe, String lucene_query_string) {
    
    // Map query_config = pe.clazz.queryConfig as Map;
    Map query_config = grails.util.GrailsClassUtils.getStaticPropertyValue(pe.getJavaClass(), 'query_config') as Map;
    if ( query_config == null ) {
      log.debug("Use default query config");
      query_config = [:]
    }

    Map builder_context=[
      aliasStack: new HashMap<String, String>(),
      queryConfig: query_config
    ]

    // Parse the lucene query and stash the parsed query in q
    // org.apache.lucene.analysis.standard.StandardAnalyzer analyzer = new org.apache.lucene.analysis.standard.StandardAnalyzer();
    Analyzer analyzer = CustomAnalyzer.builder().withTokenizer("standard").build();
 
    String default_field = query_config.defaultField ?: 'name';
    QueryParser qp = new QueryParser(default_field, analyzer)
    // qp.setLowercaseExpandedTerms(false)
    Query q = qp.parse(lucene_query_string)

    // Convert the parsed query to a deferred query
    Criterion qc = LuceneDetachedQueryBuilder.convertParsedQueryToDetachedCritria(pe, q, query_config, builder_context, dc);

    dc.add(qc)

    log.debug("Result of build: ${dc}. builder context is ${builder_context} ");

    return dc;
  }


  // see http://gorm.grails.org/6.0.x/hibernate/manual/index.html#whereQueries
  // see http://gorm.grails.org/6.0.x/hibernate/manual/index.html#detachedCriteria
  public static Criterion convertParsedQueryToDetachedCritria(PersistentEntity pe, Query q, Map query_config, Map builder_context, DetachedCriteria dc) {
    // call class.createCriteria to create the outer criteria object
    // DetachedCriteria result = c.createCriteria()
    Criterion result = null;
    log.debug("convertParsedQueryToDetachedCritria(${q}, ${query_config})");
    result = interpret(pe, q,0,builder_context, dc);
    // Set to an empty closure for now
    return result;
  }


  // See https://www.wijsmullerbros.com/2016/06/30/code-voorbeeld/ - Outer Join in Detached Criteria
  // Return a Criterion
  // Use Restrictions.and( criterionList as Criterion[] ) to make a Criterion from a list of Criterion
  // Building SQL from lucene query strings is tricky due to the required flag - a query like +A +B +C D E +F  is really saying
  // ( A AND B AND C AND F ) OR ( A AND B AND C AND F AND ( D OR E ) ) - In Lucene D OR E would add to the score of the document
  // In SQL there is no score so D or E will not change the overall output which is controlled soley by A, B C and F
  // Our strategy therefore is to group together all mandatory terms and use them in an AND then optionally
  // use them again with the optional clauses
  private static Criterion interpret(PersistentEntity pe, Query q, int depth, Map builder_context, DetachedCriteria dc) {

    Criterion result = null;

    log.debug("${'  '*depth}--> ${q} - ${q.class.name}");
    switch ( q.class ) {
      case org.apache.lucene.search.TermQuery:
        log.debug("${'  '*depth}--> TermQuery (field=${q.getTerm().field()}, term=${q.getTerm().text()}) ");
        // result = Restrictions.eq(q.getTerm().field(), q.getTerm().text());
        // result = Restrictions.ilike(q.getTerm().field(), q.getTerm().text());
        result = interpretTerm(pe, q.getTerm().field(), q.getTerm().text(), builder_context, dc);
        break;
      case org.apache.lucene.search.PhraseQuery:
        log.debug("Got phrase query - terms are ${q.getTerms()}");
        String phrase = q.getTerms().collect { it.text() }.join(' ')
        String field = q.getTerms()[0].field();
        log.debug("phrase query field: ${field}, phrase:${phrase}");
        result = interpretTerm(pe, field, phrase, builder_context, dc);
        break;
      case org.apache.lucene.search.BooleanQuery:
        log.debug("${'  '*depth}--> Boolean ${q}");
        result = interpretBooleanQuery(pe, q, depth, builder_context, dc);
        break;
      default:
        println("${'  '*depth}--> Unhandled: ${q.class.name}");
        break;
    }

    log.debug("${'  '*depth}--> result of interpret(${q},${depth}) is ${result}");
    return result;
  }

  private static Criterion interpretTerm(PersistentEntity pe, 
                                         String field, 
                                         String value, 
                                         Map builder_context, 
                                         DetachedCriteria dc) {

    log.debug("Try to resolve property ${field} on persistent entity ${pe} (${pe.class.name})");
    def propDef = DomainUtils.resolveProperty(pe, field, true)
    log.debug("DomainUtils.resolveProperty(pe,${field},true) returns ${propDef}");
    Criterion result = null;

    if (propDef) {
      if (propDef.filter) {
        def propertyType = propDef.type
        def propName = getAliasedProperty(dc, builder_context.aliasStack, field) as String

        log.debug("Result of getAliasedProperty is ${propName}");
        // result = Restrictions.ilike(field, value);
        Map query_config = grails.util.GrailsClassUtils.getStaticPropertyValue(pe.getJavaClass(), 'query_config') as Map;
        switch ( query_config?.properties?.get(field)?.mode ) {
          case 'keyword':
            log.debug("Using keyword mapping for ${field}=%${value}%");
            result = Restrictions.ilike(propName.toString(), '%'+value+'%');
            break;
          default:
            log.debug("Using simple mapping for ${field}=${value} - type is ${propDef.type}");
            switch ( propDef.type ) {
              case java.lang.Long:
                result = Restrictions.eq(propName.toString(), new Long(value));
                break;
              default:
                result = Restrictions.ilike(propName.toString(), value);
                break;
            }
            break;
        }
      }
    }

    return result;
  }

  private static final Map getAliasedProperty (def target, final Map<String, String> aliasStack, final String propname, final boolean leftJoin = false) {

    log.debug "Looking for property ${propname}"

    // Split at the dot.
    String[] levels = propname.split("\\.")

    PropertyDef ret = new PropertyDef()
    ret.putAll([
      'alias' : levels.length > 1 ? checkAlias ( target, aliasStack, levels[0..(levels.length - 2)].join('.'), leftJoin) : '',
      'property' : levels[levels.length - 1]
    ])

    ret
  }

  private static final String checkAlias(def target, final Map<String, String> aliasStack, String dotNotationString, boolean leftJoin) {

    log.debug ("Checking for ${dotNotationString}")

    def str = aliasStack[dotNotationString]
    if (!str) {

      log.debug "Full match not found..."
        
      // No alias found for exact match.
      // Start from the front and build up aliases.
      String[] props = dotNotationString.split("\\.")
      String propStr = "${props[0]}"
      String alias = aliasStack[propStr]
      String currentAlias = alias
      int counter = 1
      while (currentAlias && counter < props.length) {
        str = "${currentAlias}" 
        String test = propStr + ".${props[counter]}"
        log.debug "Testing for ${test}"
        currentAlias = aliasStack[test]
        if (currentAlias) {
          alias = currentAlias
          propStr = test
        }
        counter ++
      }

      log.debug "...propStr: ${propStr}"
      log.debug "...alias: ${alias}"

      // At this point we should have a dot notated alias string, where the aliases already been created for this query.
      // Any none existent aliases will need creating but also adding to the map for traversing.
      if (counter <= props.length) {
        // The counter denotes how many aliases were present, so we should start at the counter and create the missing
        // aliases.
        for (int i=(counter-1); i<props.length; i++) {
          String aliasVal = alias ? "${alias}.${props[i]}" : "${props[i]}"
          alias = "alias${aliasStack.size()}"

          // Create the alias.
          log.debug ("Creating alias: ${aliasVal} -> ${alias}")
          target.criteria.createAlias(aliasVal, alias, (leftJoin ? JoinType.LEFT_OUTER_JOIN : JoinType.INNER_JOIN))

          // Add to the map.
          propStr = i>0 ? "${propStr}.${props[i]}" : "${props[i]}"
          aliasStack[propStr] = alias
          log.debug ("Added quick string: ${propStr} -> ${alias}")
        }
      }

      // Set the string to the alias we ended on.
      str = alias
    }

    str
  }



  /**
   * Given a boolean query - create Criterion that reflects that query.
   * Split out mandatory and optional clauses and the formulate as ( AND_LIST_OF_MANDATORY) OR ( AND_LIST_OF_MANDATORY AND ( OR_LIST_OF_OPTIONAL ) ) 
   */
  private static Criterion interpretBooleanQuery(PersistentEntity pe, org.apache.lucene.search.BooleanQuery q, int depth, Map builder_context, DetachedCriteria dc) {
    Criterion result = null;

    List<BooleanClause> required_clauses = [];
    List<BooleanClause> optional_clauses = [];

    // If it's and crit = Restrictions.and( criterionList as Criterion[] )
    q.clauses.each { BooleanClause clause ->
      // http://lucene.apache.org/core/8_4_0/core/org/apache/lucene/search/BooleanClause.html
      log.debug("${'  '*depth}  [Clause] required:${clause.isRequired()} occur:${clause.getOccur()?.toString()?:'unset'}");
    
      // Collect all the required clauses together
      if ( clause.isRequired() ) {
        required_clauses << clause
      }
      else {
        optional_clauses << clause
      }
    }

    log.debug("interpretBooleanQuery contains ${required_clauses.size()} required clauses and ${optional_clauses.size()} optional clauses");

    if ( required_clauses.size() > 0 ) {
      // If  there were mandatory clauses - the query will be (reqiured_clauses )OR required_clauses AND ( optional OR optional OR optional )
      if ( optional_clauses.size() > 0 ) {
        log.debug("Clause has mandatory and optional sub-clauses"); // Result is ( MANDATORY_CLAUSES_ANDED ) OR ( MANDATORY_CLAUSES_ANDED AND ( OPTIONAL_CLAUSES_ORED)  )
        List<Criterion> cl = new ArrayList<Criterion>();
        // M OR ( M AND O )
        result = Restrictions.or( 
                     Restrictions.and(buildCriterionList(pe, required_clauses, depth, builder_context, dc) ),
                     Restrictions.and(
                       Restrictions.and( buildCriterionList(pe, required_clauses, depth, builder_context, dc) ),
                       Restrictions.or( buildCriterionList(pe, optional_clauses, depth, builder_context, dc) ) ) )
      }
      else {
        log.debug("Clause has mandatory but no optional sub-clauses"); // Result is ( MANDATORY_CLAUSES_ANDED )
        result = Restrictions.and( buildCriterionList(pe, required_clauses, depth, builder_context, dc) )
      }
    }
    else {
      // just the optional clauses
      log.debug("Clause is only optional sub-clauses"); // Result is ( OR_LIST_OF_OPTIONAL_CLAUSES)
      result = Restrictions.or( buildCriterionList(pe, optional_clauses, depth, builder_context, dc) )
    }

    
    log.debug("${'  '*depth} result of interpretBooleanQuery is ${result}");

    return result;
  }

  private static Criterion[] buildCriterionList(PersistentEntity pe, List<BooleanClause> boolean_clause_list, int depth, Map builder_context, DetachedCriteria dc) {
    List<Criterion> result = new ArrayList<Criterion>();
    boolean_clause_list.each { BooleanClause boolean_clause ->
      result << interpret(pe, boolean_clause.getQuery(), depth+1, builder_context, dc);
    }
    return result as Criterion[];
  }
}
