package gormfindbylqs

import grails.plugins.*
import grails.core.GrailsClass
// import grails.gorm.DetachedCriteria
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.DetachedCriteria
import org.hibernate.criterion.Projections
import org.hibernate.criterion.Subqueries


@Slf4j
class GormfindbylqsGrailsPlugin extends Plugin {

    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "4.0.1 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Gormfindbylqs" // Headline display name of the plugin
    def author = "Your name"
    def authorEmail = ""
    def description = '''\
Brief summary/description of the plugin.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/gormfindbylqs"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
//    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
//    def organization = [ name: "My Company", url: "http://www.my-company.com/" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
//    def scm = [ url: "http://svn.codehaus.org/grails-plugins/" ]

    Closure doWithSpring() { {->
            // TODO Implement runtime spring config (optional)
        }
    }

  void doWithDynamicMethods() {
    // TODO Implement registering dynamic methods to classes (optional)
    log.debug("Extending Domain classes.")

    // This closure will be added to each domain class as a method whereLuceneQueryString which accepts a persistent entity
    // and a quert closure
    def where_lqs_closure = { PersistentEntity pe, String value ->
      DetachedCriteria dc = LuceneDetachedQueryBuilder.build(pe, value);
      // log.debug("value ${value}");
      // log.debug("gc ${gc}");
      // log.debug("this.class ${this.class.name}");
      // log.debug("owner.class ${owner.class.name}");
      // log.debug("delegate.class ${delegate.class.name}");
      println("Result of build call is ${dc}");
      return dc
    }

    def find_by_lqs_closure = { PersistentEntity pe, String lqs ->
        DetachedCriteria dq = LuceneDetachedQueryBuilder.build(pe, lqs)
        Map metaParams=[:]

        // Setting one of the pagination parameters will cause list to return a grails.orm.PagedResultList which
        // contains  x.totalCount
        if ( metaParams.max == null )
          metaParams.max = 10;

        grails.orm.PagedResultList result = pe.getJavaClass().createCriteria().list(metaParams) {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

      
        return result;
    }

    def find_by_lqs_with_meta_closure = { PersistentEntity pe, String lqs, Map metaParams ->
        DetachedCriteria dq = LuceneDetachedQueryBuilder.build(pe, lqs)

        // Setting one of the pagination parameters will cause list to return a grails.orm.PagedResultList which
        // contains  x.totalCount
        if ( metaParams.max == null )
          metaParams.max = 10;

        grails.orm.PagedResultList result = pe.getJavaClass().createCriteria().list(metaParams) {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        // result.totalCount, result.resultList
        return result;
    }



 
    // (grailsApplication.getArtefacts("Domain")).each { GrailsClass gc ->
      // GrailsDomainRefdataHelpers.addMethods(pe)
    //   def targetClass = gc.clazz
    //   targetClass.metaClass.static."whereLuceneQueryString" << lqs_closure.curry(gc as PersistentEntity)
    // }

    // Use the mapping context to get all persistent entities
    grailsApplication.mainContext.getBean('grailsDomainClassMappingContext').getPersistentEntities().each { pe ->
      pe.getJavaClass().metaClass.static."whereLuceneQueryString" << where_lqs_closure.curry(pe)
      pe.getJavaClass().metaClass.static."findAllByLuceneQueryString" << find_by_lqs_closure.curry(pe)
      pe.getJavaClass().metaClass.static."findAllByLuceneQueryString" << find_by_lqs_with_meta_closure.curry(pe)
    }
  }

    void doWithApplicationContext() {
        // TODO Implement post initialization spring config (optional)
    }

    void onChange(Map<String, Object> event) {
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    void onConfigChange(Map<String, Object> event) {
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    void onShutdown(Map<String, Object> event) {
        // TODO Implement code that is executed when the application shuts down (optional)
    }
}
