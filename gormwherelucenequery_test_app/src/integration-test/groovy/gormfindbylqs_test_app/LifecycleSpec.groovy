package gormfindbylqs_test_app

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.*
import spock.lang.Specification
import gormfindbylqs_test_app.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spock.lang.Stepwise
import org.hibernate.criterion.DetachedCriteria
import org.hibernate.criterion.Projections
import org.hibernate.criterion.Subqueries



@Integration
@Stepwise
class LifecycleSpec extends Specification {

  final static Logger log = LoggerFactory.getLogger(LifecycleSpec.class);

  def setup() {
  }

  def cleanup() {
  }

  void "test create widgets" (name, descr, code, longprop, dateprop) {

    def fw = null;
    log.debug("seq#2");


    when: "We create a new widget ${name}"
      Widget.withNewTransaction {
        Widget w = new Widget(name: name, 
                              description: descr, 
                              code: code, 
                              lines:[ [lineText:name+'_line_text_1'] ],
                              longprop:longprop,
                              dateprop:dateprop
                              );
        log.debug("Created widget ${name}");
        w.save(flush:true, failOnError:true);
        log.debug("saved widget ${name}");
      }

    then: "We save that widget"
      Widget.withNewTransaction {
        fw = Widget.findByName(name);
        log.debug("Result of find : ${fw}");
        fw != null;
      }

    where:
      name|descr|code|longprop|dateprop
      'WidgetA'|'Widget A'|'alpha'|1|new Date()
      'WidgetB'|'Widget B'|'alpha'|1|new Date()
      'WidgetC'|'Widget C'|'alpha'|2|new Date()
      'WidgetD'|'Widget D'|'beta'|2|new Date()
      'TestCase05'|'TestCase05 Descr'|'beta'|3|new Date()
  }

  void testStandardWhereQuery() {

    int actual_result_count = 0;

    when: "We run a traditional where query"
      Widget.withNewTransaction {
        log.debug("RUnning query");

        // log.debug("testWhereQuery - detached criteria is ${dq.toString()}");
        def q = Widget.where {
          name == 'WidgetA'
        }

        actual_result_count = q.list().size()
      }

    then: "We get the expected result"
      actual_result_count == 1
  }

    
  void testWhereLQS(lqs, expected_count) {

    log.debug("Running whereLQS test");
    int resultsize=0;

    when: "We try to search by lqs for name:${lqs}"
      log.debug("Run query ${lqs}");
      Widget.withNewTransaction {
        DetachedCriteria dq = Widget.whereLuceneQueryString(lqs);
        log.debug("Call list on DetachedCriteria ${dq}");
    
        // SO: There is no signature of list that can take in a hibernate detatched criteria.
        // This is a snippet from some of the things that the simple lookup service does. We use
        // a wrapping "in" query with a distinct projection. This means that hits should only appear
        // once in our result set. Making it possible to paginate deserialized objects, without duplicating because
        // of multiple match points.
    
        // Add to query is a protected method of AbstractHibernateCriteriaBuilder
        // see: http://gorm.grails.org/latest/api/org/grails/orm/hibernate/query/AbstractHibernateCriteriaBuilder.html#addToCriteria(org.hibernate.criterion.Criterion)
        // We can access here because the closure is executed within the context of the AbstractHibernateCriteriaBuilder.
        // addToCriteria(Subqueries.propertyIn(
        //'id',
        // dq.setProjection(Projections.distinct(Projections.id()))
        List result = Widget.createCriteria().list {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        resultsize = result.size();
        log.debug("Query complete");
      }

    then: "That the result set is of size ${expected_count}"
      log.debug("Result of query for ${lqs} matched ${resultsize} and expected ${expected_count}");
      resultsize == expected_count

    where: 
      lqs | expected_count
      'WidgetA' | 1
      'name:WidgetA'|1
      'name:WidgetA name:WidgetB name:WidgetC'|3
      // '(name:WidgetA OR name:WidgetB ) AND name:WidgetC'|0
      // 'name:WidgetA OR name:WidgetB AND name:WidgetC'|0
      // 'name:WidgetA AND name:WidgetB OR name:WidgetC'|0
      // '( name:WidgetA AND name:WidgetB ) AND name:WidgetC'|0
      // '"Widget A"'|0
  }

  void testKeywordMapping(lqs, expected_count) {

    log.debug("Running testKeywordMapping test");
    int resultsize=0;

    when: "We try to search by lqs for name:${lqs}"
      log.debug("Run query ${lqs}");
      Widget.withNewTransaction {
        DetachedCriteria dq = Widget.whereLuceneQueryString(lqs);
        log.debug("Call list on DetachedCriteria ${dq}");
    
        List result = Widget.createCriteria().list {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        resultsize = result.size();
        log.debug("Query complete");
      }

    then: "That the result set is of size ${expected_count}"
      log.debug("Result of query for ${lqs} matched ${resultsize} and expected ${expected_count}");
      resultsize == expected_count

    where: 
      lqs | expected_count
      'description:"Widget"'|4
      'description:"Widget A"'|1
    // This line causes an exception - unsure why  'description:"Widget A"'|1
  }

  void testAliasBuild(lqs, expected_count) {

    log.debug("Running testAliasBuild test");
    int resultsize=0;

    when: "We try to search by lqs for name:${lqs}"
      log.debug("Run query ${lqs}");
      Widget.withNewTransaction {
        DetachedCriteria dq = Widget.whereLuceneQueryString(lqs);
        log.debug("Call list on DetachedCriteria ${dq}");
    
        List result = Widget.createCriteria().list {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        resultsize = result.size();
        log.debug("Query complete");
      }

    then: "That the result set is of size ${expected_count}"
      log.debug("Result of query for ${lqs} matched ${resultsize} and expected ${expected_count}");
      resultsize == expected_count

    where: 
      lqs | expected_count
      'lines.lineText:"WidgetA_line_text_1"'|1
  }

  void testBooleanSimpleAndKeyword() {
    log.debug("Running testAliasBuild test");
    int resultsize=0;

    when: "We try to search by lqs for name:${lqs}"
      log.debug("Run query ${lqs}");
      Widget.withNewTransaction {
        DetachedCriteria dq = Widget.whereLuceneQueryString(lqs);
        log.debug("Call list on DetachedCriteria ${dq}");
    
        List result = Widget.createCriteria().list {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        resultsize = result.size();
        log.debug("Query complete");
      }

    then: "That the result set is of size ${expected_count}"
      log.debug("Result of query for ${lqs} matched ${resultsize} and expected ${expected_count}");
      resultsize == expected_count

    where: 
      lqs | expected_count
      'description:Widget AND code:alpha'|3
  }

  void testLongQuery() {
    int resultsize=0;

    when: "We try to search by lqs for name:${lqs}"
      log.debug("Run query ${lqs}");
      Widget.withNewTransaction {
        DetachedCriteria dq = Widget.whereLuceneQueryString(lqs);
        log.debug("Call list on DetachedCriteria ${dq}");
    
        List result = Widget.createCriteria().list {
          addToCriteria(Subqueries.propertyIn(
            'id',
            dq.setProjection(Projections.distinct(Projections.id()))))
        };

        resultsize = result.size();
        log.debug("Query complete");
      }

    then: "That the result set is of size ${expected_count}"
      log.debug("Result of query for ${lqs} matched ${resultsize} and expected ${expected_count}");
      resultsize == expected_count

    where: 
      lqs | expected_count
      'longprop:3'|1
  }

  void testFindMethodWithMeta() {
    def resultsize=0;
    when:
      Widget.withNewTransaction {
        grails.orm.PagedResultList l = Widget.findAllByLuceneQueryString('longprop:3',[max:10])
        log.debug("Search result: ${l.totalCount}");
        resultsize=l.size();
      }
    then:
      resultsize==1;
      
  }

  void testFindMethodWithoutMeta() {
    def resultsize=0;
    when:
      Widget.withNewTransaction {
        grails.orm.PagedResultList l = Widget.findAllByLuceneQueryString('longprop:3')
        log.debug("Search result: ${l.totalCount}");
        resultsize=l.size();
      }
    then:
      resultsize==1;
      
  }

  void testFindWithoutQueryConfigBlock() {
    def resultsize=0;
    when:
      WidgetLine.withNewTransaction {
        grails.orm.PagedResultList l = WidgetLine.findAllByLuceneQueryString('lineText:WidgetA_line_text_1')
        log.debug("testFindWithoutQueryConfigBlock Search result: ${l.totalCount}");
        resultsize=l.size();
      }
    then:
      resultsize==1;
      
  }

}
