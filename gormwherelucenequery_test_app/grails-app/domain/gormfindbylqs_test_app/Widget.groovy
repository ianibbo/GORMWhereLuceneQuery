package gormfindbylqs_test_app

import org.hibernate.criterion.DetachedCriteria;

class Widget {

  String id
  String name
  String description
  String code
  Long longprop
  Date dateprop

  static hasMany = [
    lines: WidgetLine
  ]

  static mappedBy = [
    lines: 'owner'
  ]

  static query_config = [
    defaultField:'name',
    properties:[
      description:[mode:'keyword']
    ]
  ]

  public static DetachedCriteria getDetachedCriteria() {
    return DetachedCriteria.forClass(Widget.class)
  }

  static mapping = {
                   id column: 'wid_id', generator: 'uuid2', length:36
              version column: 'wid_version'
                 name column: 'wid_name'
          description column: 'wid_description'
                 code column: 'wid_code'
             longprop column: 'wid_longprop'
             dateprop column: 'wid_dateprop'
  }

  static constraints = {
           name(nullable:false)
    description(nullable:true)
           code(nullable:true)
       longprop(nullable:true)
       dateprop(nullable:true)
  }

}
