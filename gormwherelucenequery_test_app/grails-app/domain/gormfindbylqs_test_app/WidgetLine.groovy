package gormfindbylqs_test_app

class WidgetLine {

  String id
  Widget owner
  String lineText

  static belongsTo = [ owner: Widget ]

  static query_config = [
    defaultField:'lineText'
  ]


  static mapping = {
                   id column: 'wl_id', generator: 'uuid2', length:36
              version column: 'wl_version'
                owner column: 'wl_owner'
             lineText column: 'wl_text'
  }

  static constraints = {
           owner(nullable:false)
        lineText(nullable:true)
  }

}
