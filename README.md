# GORMFindByLQS

A Grails 4.0.1+ plugin that injects a method called whereLuceneQueryString into domain classes. This method
enables searching of the graph of domain objects by a lucene query syntax string, with the following semantics

* Simple scalar properties are mapped as their name name:String
* Phrase values are mapped to simple like expressions name:"A phrase"
* By setting a query config as below, the default mapping can be changed to keyword which gives much more lucene like queries - however - this is achieved using like '%value%' and should only be done where you have a mechanism like postgres GIN indexes underpinning the mapped field
* Access paths are convered into join clauses so a query against an Order like orderLines.product.productDescription:"Book" will join down the access path through order lines, products and do a like match on the product description property.

The default query config can be modified with the following static block

    static query_config = [
      properties:[
        description:[mode:'keyword']
      ]
    ]

## Extra filters

The builder will look for a static method getDetachedQuery to use as the base query for a domain. If not set this will
default to getDetachedQueryForClass(c) but if set will provide the system an opportinity to provide an outer filter
condition that is transparent to the caller.

## Developer info

If you have to maintain several environments, SDKMan is useful - we currently use the following env for this plugin

    sdk use grails 4.0.1
    sdk use java 11.0.5.j9-adpt
    sdk use gradle 5.1.1


## Dev notes

Easiest dev path - leave a console open in the test project and run

gradle cleanTest integrationTest

## Publish to the local maven repository with

gradle publishToMavenLocal


## Publush the plugin to a maven repository with

gradle publish


## to test changes

gradle cleanTest integrationTest
